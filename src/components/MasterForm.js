import React from 'react'

import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import AccountHolderDetails from '../includes/AccountHolderDetails'
import PropertySupplied from '../includes/PropertySupplied';
import BillDelivery from '../includes/BillDelivery';
import Services from '../includes/Services';
import Broadband from '../includes/Broadband';
import MedicallyDependent from '../includes/MedicallyDependent'
import PlanSummary from '../includes/PlanSummary'
import PaymentOptions from '../includes/PaymentOptions';
import NoticeToCustomer from '../includes/NoticeToCustomer';
import KeyTerms from '../includes/KeyTerms';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 'auto'
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor:"#59A946"
    
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

function getSteps() {
  return ['Account Holder Details', 'Property to be supplied', 'Bill Delivery','Services to be Supplied',
  'Broadband','Medically Dependant and Vulnerable Customers','Plan Summary','Payment','Notice to Customer - Right of Cancellation','Key terms'];
}

function getStepContent(step,values, 
  handleChange,getBirthDateChange,getAuthorisedBirthDate,getMoveInDate,getSignature,
  getElectricityMeterLocations,getGasMeterLocations,getElectricityRates,getGasRates,setBBPlan,setSelectedServices,getPassportExpiry,setAllBroadbandPlans,setBroadbandInfo,setPhonePlan) {
  switch (step) {
    case 0:
      return <AccountHolderDetails 
      values = {values} 
      handleChange={handleChange} 
      getBirthDateChange={getBirthDateChange} 
      getAuthorisedBirthDate={getAuthorisedBirthDate}
      getMoveInDate={getMoveInDate}
      getPassportExpiry={getPassportExpiry}
      />
    case 1:
      return <PropertySupplied 
      values = {values} 
      handleChange={handleChange} 
      setAllBroadbandPlans={setAllBroadbandPlans}
      />
    case 2:
      return <BillDelivery 
      values = {values} 
      handleChange={handleChange} 
     
      />;
    case 3:
        return <Services 
      values = {values} 
      handleChange={handleChange} 
      getElectricityMeterLocations={getElectricityMeterLocations}
      getGasMeterLocations={getGasMeterLocations}
      setSelectedServices={setSelectedServices}
     
      />;
      case 4:
        return <Broadband 
        values = {values} 
        setBroadbandInfo={setBroadbandInfo}
        handleChange={handleChange} 
        setBBPlan={setBBPlan}
        setPhonePlan={setPhonePlan}
      />;
    case 5:
        return <MedicallyDependent 
        values = {values} 
        handleChange={handleChange} 
       
        />;
    case 6:
        return <PlanSummary 
            values = {values} 
            handleChange={handleChange} 
            getElectricityRates={getElectricityRates}
            getGasRates={getGasRates}
           
        />;
    case 7:
        return <PaymentOptions 
            values = {values} 
            handleChange={handleChange} 
               
         />;
    case 8:
        return <NoticeToCustomer 
            values = {values} 
            handleChange={handleChange} 
                   
             />;
    case 9:
        return <KeyTerms   
            values = {values} 
            handleChange={handleChange}
            getSignature={getSignature}                    
            />;         
    default:
      return 'Unknown step';
  }
}

export default function MasterForm(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [dob,setBirthDate] = React.useState(new Date());
  const steps = getSteps();
  const [title, setTitle] = React.useState('');
  const [authdob,setauthBirthDate] = React.useState(new Date());
  const [moveInDate,setMoveInDate] = React.useState(new Date());
  const [custSignature, saveSignature] = React.useState('');
  const [eMeterLocation, setElectricityMeterLocations] = React.useState([]);
  const [gasMeterLocation, setGasMeterLocations] = React.useState([]);
  let broadbandSelected = props.values.selectedServices.broadband;
  const getSignature = (custSignature) =>{
      saveSignature(custSignature)
      props.setSignature(custSignature)
  }
  const toTop = () => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth"
    })
  }
  const handleNext = (currentSteps,steps) => {
    //check if validated with steps 
     let validationResult = props.runValidation(currentSteps);
    //  let broadbandSelected = props.values.selectedServices.broadband;
     //if validated 
    if(validationResult.status){
      // if(true){
        if(currentSteps === 1 ){
          props.getAllBroadbandPlans();
        }
        //if broadband not selected, skip section
        if(currentSteps === 3 && !broadbandSelected){
          setActiveStep((prevActiveStep) => prevActiveStep + 1);       
          
        }
          setActiveStep((prevActiveStep) => prevActiveStep + 1);
        
        
      
        toTop();
      }else{
        alert(validationResult.message)
      }  
   
  };



  const handleBack = () => {
    if(activeStep === 5 && !broadbandSelected){
      setActiveStep((prevActiveStep) => prevActiveStep - 1);          
    }
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };


  const getBirthDateChange =(dateFromChild)=>{
    setBirthDate(dateFromChild);
    props.getDOB(dateFromChild)
  };

  const getAuthorisedBirthDate =(dateFromChild)=>{
    setauthBirthDate(dateFromChild);
    props.getAuthDOB(dateFromChild)
  };

  const getMoveInDate =(dateFromChild)=>{
    setMoveInDate(dateFromChild);
    props.getMoveInDate(dateFromChild)
  };

  const getElectricityMeterLocations=(locationFromChild)=>{
    setElectricityMeterLocations(locationFromChild)
    props.setElectricityMeterLocation(locationFromChild)
  }
  const getGasMeterLocations=(locationFromChild)=>{
    setGasMeterLocations(locationFromChild)
    props.setGasMeterLocation(locationFromChild)
  }


  const getElectricityRates=(ratesFromChild)=>{
    props.setElectricityRates(ratesFromChild)
  }
  
  const getGasRates=(gasRatesFromChild)=>{
    props.setGasRates(gasRatesFromChild)
  }

  return (
    <div className={classes.root}>
      
      <Stepper activeStep={activeStep} orientation="vertical" >
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
            {getStepContent(index,props.values, props.handleChange,getBirthDateChange,
              getAuthorisedBirthDate, getMoveInDate,getSignature,getElectricityMeterLocations,getGasMeterLocations,getElectricityRates,getGasRates,props.setBBPlan,props.setSelectedServices,props.getPassportExpiry,props.setAllBroadbandPlans,props.setBroadbandInfo,props.setPhonePlan)}
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={(e) => handleNext(activeStep,steps)}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography style={{fontSize:"1.8rem"}}>All steps completed - you&apos;re finished</Typography>
          <Button onClick={()=>props.handleSubmission(activeStep,steps)} className={classes.button} id='submitFormButton' style={{fontSize:"1.5rem"}}>
            Send form
          </Button>
          
        </Paper>
      )}
     
    </div>
  );
}
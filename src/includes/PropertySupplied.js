import React from 'react'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from "@material-ui/core/TextField";
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';

export default function PropertySupplied(props) {


 
    return (
        <div className="container">
            <div className="row">
            
                <div className="col-12">
                <TextField required 
                    name="unitOrStNo" 
                    label="Unit / Street No" 
                    variant="standard"  
                    style={{width:"100px"}}
                    value={props.values.unitOrStNo} 
                    onChange={props.handleChange('unitOrStNo')} 
                    error={!props.values.unitOrStNo} 
                    helperText={!props.values.unitOrStNo ? 'This is required' : ' '}/>
                   <TextField required 
                    style={{width:"320px"}}
                    name="streetAddress" 
                    label="Street Name" 
                    variant="standard"  
                    value={props.values.streetAddress} 
                    onChange={props.handleChange('streetAddress')} 
                    error={!props.values.streetAddress} 
                    helperText={!props.values.streetAddress ? 'This is required' : ' '}/>
                    <TextField
                    name="suburb" 
                    label="Suburb" 
                    variant="standard"  
                    value={props.values.suburb} 
                    onChange={props.handleChange('suburb')} />
                  
                   
                </div>
                <div className="col-12">
                <TextField
                required
                    name="townOrCity" 
                    label="Town/City" 
                    variant="standard"  
                    value={props.values.townOrCity} 
                    onChange={props.handleChange('townOrCity')}
                    error={!props.values.townOrCity} 
                    helperText={!props.values.townOrCity ? 'This is required' : ' '} />
                    <TextField
                      style={{width:"100px"}}
                    required
                    name="postcode"
                    type="number" 
                    label="Postcode" 
                    variant="standard"  
                    value={props.values.postcode} 
                    onChange={props.handleChange('postcode')}
                    error={!props.values.postcode} 
                    helperText={!props.values.postcode ? 'This is required' : ' '}
                    inputProps={{max:9999}} />
                    
                </div>
                <div className="col-12">
                <TextField
                    required
                    name="ICP" 
                    label="ICP" 
                    variant="standard"  
                    value={props.values.ICP} 
                    onChange={props.handleChange('ICP')}
                    error={!props.values.ICP} 
                    helperText={!props.values.ICP ? 'This is required' : ' '}
                    inputProps={
                        {maxLength: 15}
                      }
                    helperText="15 digits"/>
                </div>
                <div className="col-12">
                <FormLabel component="legend"><h6 style={{marginTop:"1rem"}}>Postal Address</h6></FormLabel>
                <FormControlLabel
                    control={<Checkbox checked={props.values.sameAddress} onChange={props.handleChange('sameAddress')} name="sameAddress" />}
                    label="Postal address is same as above"
                  />
                </div>
                <div className="col-12" style={{padding:"0"}}>
                {!props.values.sameAddress && 
                <FormControl component="fieldset">
                    
                    <div className="col-12">
                        <TextField required 
                        name="unitOrStNoPost" 
                        label="Unit / Street No" 
                        variant="standard"  
                        style={{width:"100px"}}
                        value={props.values.unitOrStNoPost} 
                        onChange={props.handleChange('unitOrStNoPost')} 
                        error={!props.values.unitOrStNoPost} 
                        helperText={!props.values.unitOrStNoPost ? 'This is required' : ' '}
                       />
                        <TextField required 
                        style={{width:"320px"}}
                        name="streetAddressPost" 
                        label="Street Name" 
                        variant="standard"  
                        value={props.values.streetAddressPost} 
                        error={!props.values.streetAddressPost} 
                        helperText={!props.values.streetAddressPost ? 'This is required' : ' '}
                        onChange={props.handleChange('streetAddressPost')} />
                        <TextField
                        name="suburb" 
                        label="Suburb" 
                        variant="standard"  
                        value={props.values.suburbPost} 
                        onChange={props.handleChange('suburbPost')} />
                      <TextField
                    required
                        name="townOrCityPost" 
                        label="Town/City" 
                        variant="standard"  
                        value={props.values.townOrCityPost} 
                        error={!props.values.townOrCityPost} 
                        helperText={!props.values.townOrCityPost ? 'This is required' : ' '}
                        onChange={props.handleChange('townOrCityPost')} />
                        <TextField
                          style={{width:"100px"}}
                        required
                        name="postcodePost"
                        type="number" 
                        label="Postcode" 
                        variant="standard"  
                        value={props.values.postcodePost} 
                        error={!props.values.postcodePost} 
                        helperText={!props.values.postcodePost ? 'This is required' : ' '}
                        onChange={props.handleChange('postcodePost')}
                        inputProps={{max:9999}} />
                         </div>
                    </FormControl>}
                </div>
            </div>
            
        </div>
    )
}

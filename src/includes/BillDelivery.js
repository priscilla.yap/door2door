import React from 'react'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import validator from 'validator';
import TextField from "@material-ui/core/TextField";
import Checkbox from '@material-ui/core/Checkbox';

export default function BillDelivery(props) {

    const[validEmail, validateEmail] = React.useState(false);
  
    const validateEmailField=(input)=>{
        if(validator.isEmail(input)){
            validateEmail(false)
        }else{
            validateEmail(true)
        }
    }
    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                        <RadioGroup row aria-label="onlineOrPost" label="Online or Post bill" name="onlineOrPost" value={props.values.onlineOrPost} onChange={props.handleChange('onlineOrPost')} defaultValue={props.values.onlineOrPost}>
                            <FormControlLabel value="online" control={<Radio color="primary" />} label="Online Bill" />
                            <FormControlLabel value="post" control={<Radio color="primary" />} label="Post (additional charges may apply)" />
                        </RadioGroup>
                        {props.values.onlineOrPost==="post"
                        // ?     <FormControl component="fieldset" > 
                        // <FormControlLabel
                        //     control={<Checkbox checked={props.values.sameEmail} onChange={props.handleChange('sameEmail')} name="sameEmail" />}
                        //     label="Same email entered as above"
                        //   />
                        // <TextField name="onlineEmail" label="Email Address" variant="standard" margin="normal"  
                        // style={{marginTop:"0"}}
                        // value={props.values.onlineEmail} onChange={props.handleChange('onlineEmail')} error={validEmail} 
                        // helperText={validEmail ? 'Please enter a valid email' : ' '}  onBlur={(e)=> validateEmailField(e.target.value)}/>
                        
                        
                        //    </FormControl>
                       
                        // :
                        &&
                        <FormControl component="fieldset">
                        <TextField required 
                        name="unitOrStNoPost" 
                        label="Unit / Street No" 
                        variant="standard"  
                        value={props.values.unitOrStNoPost} 
                        onChange={props.handleChange('unitOrStNoPost')} 
                        error={!props.values.unitOrStNoPost} 
                        helperText={!props.values.unitOrStNoPost ? 'This is required' : ' '}
                       />
                        <TextField required 
                        style={{width:"320px"}}
                        name="streetAddressPost" 
                        label="Street Address" 
                        variant="standard"  
                        value={props.values.streetAddressPost} 
                        onChange={props.handleChange('streetAddressPost')}
                        error={!props.values.streetAddressPost} 
                        helperText={!props.values.streetAddressPost ? 'This is required' : ' '} />
                        <TextField
                        name="suburb" 
                        label="Suburb" 
                        variant="standard"  
                        value={props.values.suburbPost} 
                        onChange={props.handleChange('suburbPost')}
                         />
                      
                       
                  
                    <TextField
                    required
                        name="townOrCityPost" 
                        label="Town/City" 
                        variant="standard"  
                        value={props.values.townOrCityPost} 
                        onChange={props.handleChange('townOrCityPost')} 
                        error={!props.values.townOrCityPost} 
                        helperText={!props.values.townOrCityPost ? 'This is required' : ' '}
                        />
                        <TextField
                          style={{width:"100px"}}
                        required
                        name="postcodePost"
                        type="number" 
                        label="Postcode" 
                        variant="standard"  
                        value={props.values.postcodePost} 
                        onChange={props.handleChange('postcodePost')}
                        error={!props.values.postcodePost} 
                        helperText={!props.values.postcodePost ? 'This is required' : ' '}
                        inputProps={{max:9999}} />
                         
                    </FormControl>
                   
                        }
                </div>
            </div>
            
        </div>
    )
}

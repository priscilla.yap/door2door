import React from 'react'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
export default function PaymentOptions(props) {
    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <FormLabel component="legend">How would you like to pay your bill?</FormLabel>
                    <RadioGroup row aria-label="paymentMethod" label="Payment Method" name="paymentMethod" value={props.values.paymentMethod} onChange={props.handleChange('paymentMethod')} defaultValue={props.values.paymentMethod}>
                        <FormControlLabel value="Direct Debit" control={<Radio color="primary" />} label="Direct Debit" />
                        <FormControlLabel value="Smooth Pay" control={<Radio color="primary" />} label="Smooth Pay" />
                        <FormControlLabel value="Credit Card" control={<Radio color="primary" />} label="Credit Card" />
                        <FormControlLabel value="Other" control={<Radio color="primary" />} label="Other" />
                    </RadioGroup>
                </div>
            </div>
            
        </div>
    )
}

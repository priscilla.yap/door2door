import React from 'react'
import axios from 'axios';
import BroadbandSummaryRow from './BroadbandSummaryRow';
import BroadbandQuestions from './BroadbandQuestions';
import { green } from '@material-ui/core/colors';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormLabel from '@material-ui/core/FormLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

export default function Broadband(props) {
    const [broadbandPlan, setBroadbandPlan] = React.useState(props.values.broadbandPlan);
    const [broadbandPlanName, setBroadbandPlanName] = React.useState(props.values.broadbandPlan[0].plan);
    const [broadbandPlans, getBroadbandPlans] = React.useState(props.values.broadbandPlans); 
    const [loadingStatus, setLoadingStatus] = React.useState(props.values.loadingBroadband); 
    

   
    React.useEffect(()=>{
        props.setBBPlan(broadbandPlan);
        setLoadingStatus(props.values.loadingBroadband)

        if(props.values.broadbandPlans instanceof Array && props.values.broadbandPlans.length !== 0){
            setLoadingStatus(false)
            getBroadbandPlans(props.values.broadbandPlans)
        }

      
      },[broadbandPlan,props.values.broadbandPlans])
    
    const setBBPlan=(bbPlan)=>{
        setBroadbandPlan(bbPlan)
       
        if(broadbandPlans instanceof Array && broadbandPlans.length !== 0){
            setBroadbandPlanName(bbPlan[0].plan)
            
        }   
    }

    const handleBroadbandDetails=(value)=> {
        props.setBroadbandInfo(value)
      }

    const displaybroadbandPlans=()=>{
        let defaultPlanName = broadbandPlanName
       
        if(broadbandPlans instanceof Array && broadbandPlans.length !== 0){
           
                //sort Regular plan to bottom
                if(broadbandPlans[0].Title === 'Regular'){
                    const regularPlan = broadbandPlans.shift()
                    broadbandPlans.push(regularPlan)
                }
                return broadbandPlans.map(function(object, i){
                
                    return  <BroadbandSummaryRow obj={object} key={i} index={i} defaultPlan={defaultPlanName}  setBBPlan={setBBPlan}></BroadbandSummaryRow>
                           
                    
                })
         }else{
            return <React.Fragment>
                <p className="MuiFormLabel-root Mui-error">We are unable to find any broadband plans available at this address.<br/>Please enter preferred broadband plan in box below.</p>
                <FormLabel component="legend">Plan name</FormLabel>
                    <TextareaAutosize 
                    placeholder="Please enter a plan name."
                    aria-label="Plan Details" 
                    style={{width:"300px",marginBottom:"1rem"}}
                    rowsMin={3} 
                    value={props.values.desiredPlanDetails} 
                    onChange={props.handleChange('desiredPlanDetails')}/>
                </React.Fragment>
        }
    }

    const getSuburb = () =>{
        
       return props.values.suburb && props.values.suburb+ ', '
    }


    return (
        <div className="container">
            <div className="row">
            {props.values.streetAddress && <p className="w-100"><strong>Address:</strong> <span>{props.values.unitOrStNo + ' ' +props.values.streetAddress + ', ' + getSuburb() +  props.values.townOrCity}</span></p>} 
            {/* set loading if not loaded yet */}
            {props.values.loadingBroadband ? <CircularProgress fontSize="small"  style={{ color: green[500] }}/>: displaybroadbandPlans()}  
            </div>
           
             {/* BroadbandQuestions set loading if not loaded yet */}
            {!props.values.loadingBroadband && <BroadbandQuestions values={props.values} handleBroadbandDetails={handleBroadbandDetails} setPhonePlan={props.setPhonePlan} /> }     
            
        </div>
    )
}